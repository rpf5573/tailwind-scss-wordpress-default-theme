(($) => {
    $(document).ready(function() {
        const $mobileMenuTrigger = $('#mobile-menu-open-trigger');
        $mobileMenuTrigger.on('click', () => {
            // add 'mobile-menu-opened' class to body;
            $('body').addClass('mobile-menu-opened');
        });

        const $offcanvasCloseTrigger = $('#mobile-menu-close-trigger');
        $offcanvasCloseTrigger.on('click', () => {
            // remove 'mobile-menu-opened' class to body;
            $('body').removeClass('mobile-menu-opened');
        });

        // 외부 클릭시 닫기
        $('.offcanvas-nav-container').click(function(event) {
            // 'offcanvas-inner' 요소를 클릭했는지 확인하기 위해 이벤트 대상을 검사합니다
            if (!$(event.target).closest('.offcanvas-inner').length) {
                // 'offcanvas-inner'가 클릭되지 않았을 때의 동작을 여기에 추가합니다
                $('body').removeClass('mobile-menu-opened');
            }
        });
    });
})(jQuery);

// accordion
(($) => {
    // Include jQuery library
    $(document).ready(function() {
        // '.toggle-sub-menu' 클릭 이벤트를 처리합니다
        $('.toggle-sub-menu').click(function(event) {
            event.preventDefault(); // 기본 동작 방지
            // 클릭된 요소의 부모의 부모 요소인 '.menu-item'에서 '.sub-menu'를 찾아 토글 애니메이션을 적용합니다
            $(this).closest('.menu-item').find('.sub-menu').slideToggle();

            $(this).find('.svg-icon-down, .svg-icon-up').toggle();
        });
    });
})(jQuery);

// Login Modal
(($) => {
    document.addEventListener('DOMContentLoaded', function() {
        const $loginBtn = document.querySelector('.login-button > a');
        $loginBtn.addEventListener('click', function(e) {
            e.preventDefault();
            const $loginModal = $('.login-modal');
            $loginModal.modal({
                fadeDuration: 100,
                closeText: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>'
            });
        });
    });
})(jQuery);

// 사용자 이미지 preview
(($) => {
    document.addEventListener('DOMContentLoaded', function() {
        const imageInput = document.getElementById('profile_image');
    
        imageInput.addEventListener('change', function() {
            if (this.files && this.files[0]) {
                const reader = new FileReader();
    
                reader.onload = function(e) {
                    document.getElementById('profile_image_preview').src = e.target.result;
                };
    
                reader.readAsDataURL(this.files[0]);
            }
        });
    });    
})(jQuery);

// 회원 정보 수정 api
(($) => {
    $(document).ready(function() {
        $('#my_account_form').on('submit', function(e) {
            e.preventDefault();

            const $indicator = $('.loading-indicator');
            $indicator.removeClass('hidden');

            const formData = new FormData(this);
            formData.append('action', 'save_my_account');

            $.ajax({
                url: ex_global_obj.ajaxurl,
                type: 'POST',
                data: formData,
                contentType: false,  // Required for FormData
                processData: false,  // Required for FormData
                success: function(response) {
                    console.log(response);

                    const isSuccessful = response.success;
                    if (!isSuccessful) {
                        $indicator.addClass('hidden');
                        alert(response.data);
                        return;
                    }
                    $indicator.addClass('hidden');
                    alert("회원 정보가 수정되었습니다.");
                    // window.location.href = window.location.href;
                },
                error: function(error) {
                    console.log(error);
                    $indicator.addClass('hidden');
                    alert("회원 정보 수정에 문제가 발생했습니다.");
                }
            });
        });
    });
})(jQuery);

// 행사 사진 다운로드 버튼 추가
(($) => {
    $(document).ready(function() {
        // Select all gallery items
        $('.kboard-gallery-item').each(function() {
            var $thumbnail = $(this).find('.kboard-gallery-thumbnail');
            var itemLink = $(this).find('img').attr('src'); // Get the image source

            // Regular expression to match the pattern '-numberxnumber'
            var sizePattern = /-\d+x\d+/g;
            var matches = itemLink.match(sizePattern);

            // If matches are found, remove the last occurrence
            if (matches && matches.length > 0) {
                var lastMatch = matches[matches.length - 1];
                var lastIndex = itemLink.lastIndexOf(lastMatch);
                var originalItemLink = itemLink.substring(0, lastIndex) + itemLink.substring(lastIndex + lastMatch.length);

                // Create the download button
                var $downloadButton = $('<a>', {
                    text: 'Download', // Text for the button
                    href: originalItemLink, // Set the download link to the modified image
                    class: 'download-button', // Add class for styling
                    css: {
                        position: 'absolute',
                        right: '10px',
                        bottom: '10px'
                    },
                    target: '_blank',
                    download: ''
                });

                // Append the button to the thumbnail
                $thumbnail.append($downloadButton);
            }
        });
    });
})(jQuery);

// 회원 정보 수정 페이지에서 비밀번호 보기 버튼 toggle
// 비밀번호 유효성 검사
(($) => {
    document.addEventListener('DOMContentLoaded', () => {
        // Select the password input and the toggle checkbox
        const passwordInput = document.getElementById('native_password');
        const toggleCheckbox = document.getElementById('toggle-password');

        // Password validation function
        function validatePassword(password) {
            // Regex for password validation: 8-16 characters, including letters and numbers
            const regex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,16}$/;
            return regex.test(password);
        }

        const originalBorderColor = passwordInput.style.borderColor;
        const originalBorderWidth = passwordInput.style.borderWidth;

        // Add event listener to validate password on input change
        passwordInput.addEventListener('input', function() {
            if (validatePassword(passwordInput.value)) {
                // If valid, you can set some visual indication, like changing the border color
                passwordInput.style.borderColor = originalBorderColor;
                passwordInput.style.borderWidth = originalBorderColor;
            } else {
                passwordInput.style.borderColor = 'red';
                passwordInput.style.borderWidth = '3px';
            }
        });

        toggleCheckbox.addEventListener('change', (event) => {
            if(event.target.checked) {
                passwordInput.type = 'text';
            } else {
                passwordInput.type = 'password';
            }
        });
    });
})(jQuery);

// .login-modal 내부에 .rwmb-error의 textContent가 비어있지 않다면, login-modal을 띄웁니다
(($) => {
    document.addEventListener('DOMContentLoaded', () => {
        const $loginModal = $('.login-modal');
        const $loginModalError = $loginModal.find('.rwmb-error');
        if ($loginModalError.text().trim() !== '') {
            $loginModal.modal({
                fadeDuration: 100,
                closeText: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"> </line><line x1="6" y1="6" x2="18" y2="18"></line></svg>',
            });
        }
    });
})(jQuery);