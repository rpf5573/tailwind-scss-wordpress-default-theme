// main entry point
// include your assets here

import './assets/js/lib-jquery-modal.js';

// get styles
import "./assets/scss/style.scss";

// get scripts
import './assets/js/scripts.js';
// import './assets/js/mobile-menu.js';