<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
    exit;  
    
/*
 * VITE & Tailwind JIT development
 * Inspired by https://github.com/andrefelipe/vite-php-setup
 *
 */

// dist subfolder - defined in vite.config.json
define('DIST_DEF', 'dist');

// defining some base urls and paths
define('DIST_URI', get_stylesheet_directory_uri() . '/' . DIST_DEF);
define('DIST_PATH', get_stylesheet_directory() . '/' . DIST_DEF);

// js enqueue settings
define('JS_DEPENDENCY', array('jquery')); // array('jquery') as example
define('JS_LOAD_IN_FOOTER', true); // load scripts in footer?

// deafult server address, port and entry point can be customized in vite.config.json
define('VITE_SERVER', 'http://localhost:3000');
define('VITE_ENTRY_POINT', '/main.js');

// enqueue hook
add_action( 'wp_enqueue_scripts', function() {

    wp_enqueue_script('jquery');
    
    if (defined('IS_VITE_DEVELOPMENT') && IS_VITE_DEVELOPMENT === true) {

        function vite_head_module_hook() {
            ob_start();
            ?>
            <script>
                const ex_global_obj = {
                    'ajaxurl': '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                    'siteurl': '<?php echo get_site_url(); ?>',
                    'templateurl': '<?php echo get_template_directory_uri(); ?>',
                    'stylesheeturl': '<?php echo get_stylesheet_directory_uri(); ?>',
                    'disturl': '<?php echo DIST_URI; ?>',
                    'distpath': '<?php echo DIST_PATH; ?>',
                };
            </script>
            <script type="module" crossorigin src="<?php echo VITE_SERVER . VITE_ENTRY_POINT; ?>"></script>
            <?php
            $output = ob_get_clean();
            echo $output;
        }
        
        
        add_action('wp_head', 'vite_head_module_hook');

    } else {

        // production version, 'npm run build' must be executed in order to generate assets
        // ----------

        // read manifest.json to figure out what to enqueue
        $manifest = json_decode( file_get_contents( DIST_PATH . '/.vite/manifest.json'), true );

        function_exists('ray') && ray('manifest', $manifest);
        
        
        // is ok
        if (is_array($manifest)) {
            
            // get first key, by default is 'main.js' but it can change
            $manifest_key = array_keys($manifest);
            if (isset($manifest_key[0])) {
                
                // enqueue CSS files
                foreach(@$manifest[$manifest_key[0]]['css'] as $css_file) {
                    wp_enqueue_style( 'main', DIST_URI . '/' . $css_file );
                }
                
                // enqueue main JS file
                $js_file = @$manifest[$manifest_key[0]]['file'];
                if ( ! empty($js_file)) {
                    echo '<script>var ex_ajaxurl = "' . admin_url( 'admin-ajax.php' ) . '";</script>';
                    wp_localize_script('main', "ex_global_obj", array(
                        'ajaxurl' => admin_url( 'admin-ajax.php' ),
                        'siteurl' => get_site_url(),
                        'templateurl' => get_template_directory_uri(),
                        'stylesheeturl' => get_stylesheet_directory_uri(),
                        'disturl' => DIST_URI,
                        'distpath' => DIST_PATH,
                    ));
                    wp_enqueue_script( 'main', DIST_URI . '/' . $js_file, JS_DEPENDENCY, '', JS_LOAD_IN_FOOTER );
                }
                
            }

        }

    }

});