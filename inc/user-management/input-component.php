<div class="input-container tw-flex-1 <?php echo isset($args['additional_class']) ? $args['additional_class'] : ''; ?>">
    <input type="<?php echo isset($args['input_type']) ? $args['input_type'] : 'text'; ?>" id="<?php echo isset($args['input_id']) ? $args['input_id'] : ''; ?>" name="<?php echo isset($args['input_name']) ? $args['input_name'] : ''; ?>" class="tw-w-full tw-px-3 tw-py-2 tw-border tw-rounded" placeholder="<?php echo isset($args['placeholder']) ? $args['placeholder'] : ''; ?>" value="<?php echo isset($args['value']) ? $args['value'] : ''; ?>">
</div>
