<?php
require_once __DIR__ . '/api.php';
require_once __DIR__ . '/metabox.php';

// My Account Shortcode
add_shortcode( 'ex_my_account', function() {
    ob_start();
    $user_id = get_current_user_id();
    // check if the user is logged in
    if ( ! $user_id ) {
        return '<p>You must be logged in to view this page.</p>';
    }

    $fields = [
        'profile_image_url', 'course_name', 'admission_date', 'batch', 'completion_date',
        'mobile_phone', 'home_address_1', 'home_address_2', 'home_postcode', 'mailing_address',
        'workplace_name', 'position', 'work_address_1', 'work_address_2', 'work_postcode',
        'native_password', 'home_phone', 'department', 'work_phone'
    ];

    $field_values = [];
    foreach ($fields as $field) {
        $field_values[$field] = WPM_Helpers::mb_get_user_meta([
            'user_id' => $user_id,
            'field_id' => $field
        ]);
    }

    if (!isset($field_values['profile_image_url']) || empty($field_values['profile_image_url'])) {
        $field_values['profile_image_url'] = WPM_Helpers::get_image_url('default-profile-image.png');
    }

    // 몇몇 정보들은 워드프레스 기본 필드에서 가져옵니다
    // get user email
    $field_values['email'] = get_userdata($user_id)->user_email;

    // get user's first name and last name and insert it into full_name field
    $user_info = get_userdata($user_id);
    $field_values['full_name'] = $user_info->last_name . $user_info->first_name;

    ?>
    <form id="my_account_form" class="tw-flex tw-flex-col tw-gap-12">
        <script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
        <?php wp_nonce_field( 'my_account_action', 'my_account_nonce' ); ?>

        <div class="base-inf">
            <h2 class="tw-font-bold tw-pb-3 tw-text-3xl">기본정보</h2>
            <div class="tw-mx-auto tw-border-t-2 tw-border-gray-400 tw-border-b">
                <div class="tw-flex tw-flex-col">

                    <!-- 프로필 이미지 -->
                    <div class="tw-flex tw-flex-col tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'profile_image', 'label_text' => '프로필 이미지')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center">
                                <div class="tw-flex tw-flex-col tw-ml-5 tw-py-3">
                                    <img id="profile_image_preview" src="<?php echo $field_values['profile_image_url']; ?>" alt="프로필 사진" class="tw-w-full tw-h-full tw-mb-2 tw-block">
                                    <input type="file" id="profile_image" name="profile_image" accept="image/*" class="!tw-text-xl">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 이름: 전체 행 -->
                    <div class="tw-flex tw-flex-col tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'full_name', 'label_text' => '이름')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'full_name', 'input_name' => 'full_name', 'value' => $field_values['full_name'])); ?>
                                <span><?php echo $field_values['full_name']; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 과정명, 기수: 반쪽-반쪽 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'course_name', 'label_text' => '과정명')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'course_name', 'input_name' => 'course_name', 'value' => $field_values['course_name'])); ?>
                                <span><?php echo $field_values['course_name']; ?></span>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'batch', 'label_text' => '기수')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'batch', 'input_name' => 'batch', 'value' => $field_values['batch'])); ?>
                                <span><?php echo $field_values['batch']; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 입학일자, 수료일자: 반쪽-반쪽 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'admission_date', 'label_text' => '입학일자')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'date', 'input_id' => 'admission_date', 'input_name' => 'admission_date', 'value' => $field_values['admission_date'])); ?>
                                <span><?php echo $field_values['admission_date']; ?></span>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'completion_date', 'label_text' => '수료일자')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'date', 'input_id' => 'completion_date', 'input_name' => 'completion_date', 'value' => $field_values['completion_date'])); ?>
                                <span><?php echo $field_values['completion_date']; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 이메일, 비밀번호: 반쪽-반쪽 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'email', 'label_text' => '이메일')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'email', 'input_id' => 'email', 'input_name' => 'email', 'value' => $field_values['email'])); ?>
                                <span><?php echo $field_values['email']; ?></span>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'native_password', 'label_text' => '비밀번호')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5 tw-gap-6 tw-py-3">
                                <div class="left">
                                    <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'native_password', 'input_id' => 'native_password', 'input_name' => 'native_password', 'input_type' => 'password')); ?>
                                    <div>
                                        <span class="tw-text-xl tw-text-black">(8~16자의 영문 글자와 숫자의 혼용된 조합이어야 함)</span>
                                    </div>
                                </div>
                                <div class="right">
                                    <!-- 비밀번호 표시 toggle -->
                                    <div class="toggle-password tw-flex tw-gap-2">
                                        <input type="checkbox" id="toggle-password">
                                        <label for="toggle-password" class="tw-flex tw-items-center tw-cursor-pointer">
                                            <span class="tw-mr-2 tw-text-2xl">비밀번호 표시</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 휴대전화, 집전화: 반쪽-반쪽 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'mobile_phone', 'label_text' => '휴대전화')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'mobile_phone', 'input_name' => 'mobile_phone', 'value' => $field_values['mobile_phone'])); ?>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'home_phone', 'label_text' => '집전화')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'home_phone', 'input_name' => 'home_phone', 'value' => $field_values['home_phone'])); ?>
                            </div>
                        </div>
                    </div>


                    <div class="tw-border-b tw-border-gray-200">
                        <!-- 자택주소 -->
                        <?php get_template_part('inc/user-management/address-component', null, array(
                            'label_text' => '자택주소',
                            'input_id' => 'home_address',
                            'postcode_id' => 'home_postcode',
                            'postcode_name' => 'home_postcode',
                            'postcode_value' => $field_values['home_postcode'],
                            'address1_id' => 'home_address_1',
                            'address1_name' => 'home_address_1',
                            'address1_value' => $field_values['home_address_1'],
                            'address2_id' => 'home_address_2',
                            'address2_name' => 'home_address_2',
                            'address2_value' => $field_values['home_address_2'],
                        )); ?>
                    </div>

                    <!-- 우편물수령지 -->
                    <div class="tw-flex tw-gap-6 tw-items-center">
                        <div class="tw-w-[120px] tw-flex tw-justify-center tw-items-center">
                            <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'mailing_address', 'label_text' => '우편물 수령지', 'additional_class' => 'tw-h-full')); ?>
                        </div>
                        <div class="tw-flex-1 tw-flex tw-gap-6">
                            <!-- 자택 -->
                            <div class="tw-flex">
                                <input type="radio" id="mailing_address_home" name="mailing_address" value="home" class="tw-mr-2" <?php checked($field_values['mailing_address'], 'home'); ?>>
                                <label for="mailing_address_home">자택</label>
                            </div>
                            <!-- 직장 -->
                            <div class="tw-flex">
                                <input type="radio" id="mailing_address_work" name="mailing_address" value="work" class="tw-mr-2" <?php checked($field_values['mailing_address'], 'work'); ?>>
                                <label for="mailing_address_work">직장</label>
                            </div>
                            <!-- 온라인 -->
                            <div class="tw-flex">
                                <input type="radio" id="mailing_address_online" name="mailing_address" value="online" class="tw-mr-2" <?php checked($field_values['mailing_address'], 'online'); ?>>
                                <label for="mailing_address_online">온라인</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="job-info">
            <h2 class="tw-font-bold tw-pb-3 tw-text-3xl">직장정보</h2>
            <div class="tw-mx-auto tw-border-t-2 tw-border-gray-400 tw-border-b">
                <div class="tw-flex tw-flex-col">
                    <!-- 직장명, 부서명 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'workplace_name', 'label_text' => '직장명')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'workplace_name', 'input_name' => 'workplace_name', 'value' => $field_values['workplace_name'])); ?>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'department', 'label_text' => '부서명')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'department', 'input_name' => 'department', 'value' => $field_values['department'])); ?>
                            </div>
                        </div>
                    </div>

                    <!-- 직위, 직장전화 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'position', 'label_text' => '직위')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'position', 'input_name' => 'position', 'value' => $field_values['position'])); ?>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'work_phone', 'label_text' => '직장전화')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'work_phone', 'input_name' => 'work_phone', 'value' => $field_values['work_phone'])); ?>
                            </div>
                        </div>
                    </div>

                    <!-- 직장주소 -->
                    <?php get_template_part('inc/user-management/address-component', null, array(
                        'label_text' => '직장주소',
                        'input_id' => 'work_address',
                        'postcode_id' => 'work_postcode',
                        'postcode_name' => 'work_postcode',
                        'postcode_value' => $field_values['work_postcode'],
                        'address1_id' => 'work_address_1',
                        'address1_name' => 'work_address_1',
                        'address1_value' => $field_values['work_address_1'],
                        'address2_id' => 'work_address_2',
                        'address2_name' => 'work_address_2',
                        'address2_value' => $field_values['work_address_2'],
                    )); ?>
                </div>
            </div>
        </div>
        <div class="tw-flex tw-justify-center tw-py-4">
            <button type="submit" id="save_my_account" class="tw-bg-purple-900 tw-text-3xl tw-text-white tw-rounded-2xl tw-py-6 tw-px-12 tw-font-bold">
                <span class="tw-mt-[-2px] tw-block">저장</span>
            </button>
        </div>
    </form>
    <div class="loading-indicator hidden">
        <div class="inner">
            <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                    <div class="pswp__preloader__donut"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
    document.addEventListener('DOMContentLoaded', function() {
        // Find all postcode search buttons
        const searchButtons = document.querySelectorAll('.postcode-search-button');
        searchButtons.forEach(function(button, index) {
            button.addEventListener('click', function() {
                new daum.Postcode({
                    oncomplete: function(data) {
                        // Code to be executed when a search result is clicked in the popup
                        var roadAddr = data.roadAddress; // Road address
                        var jibunAddr = data.jibunAddress; // Jibun address

                        // Using index to target specific instances of the inputs
                        var container = button.closest('.address-component');
                        container.querySelector('.postcode-input').value = data.zonecode; // Postcode
                        container.querySelector('.address1-input').value = roadAddr; // Road address
                        container.querySelector('.address2-input').value = jibunAddr; // Jibun address
                    }
                }).open();
            });
        });
    });
</script>
<?php
    return ob_get_clean();
} );

function ex_add_upload_files_to_all_roles() {
    global $wp_roles;
    if (!isset($wp_roles)) {
        $wp_roles = new WP_Roles();
    }

    foreach ($wp_roles->role_objects as $role) {
        if (!$role->has_cap('upload_files')) {
            $role->add_cap('upload_files', true);
            $role->add_cap('unfiltered_upload', true);
        }
    }
}
add_action('admin_init', 'ex_add_upload_files_to_all_roles');

add_action( 'edit_user_profile_update', function() {
    function_exists('ray') && ray('edit_user_profile_update', 'edit_user_profile_update');

    // when save user profile, and have new password, then save the new user password to 'native_password' metabox field
    if (isset($_POST['pass1']) && !empty($_POST['pass1'])) {
        $user_id = get_current_user_id();
        $value = $_POST['pass1'];
        $params = array(
            'user_id' => $user_id, // the ID of the user to update
            'field_id' => 'native_password',    // the ID of the field to update
            'value' => sanitize_text_field( $value ) // the new value for the field
        );
        WPM_Helpers::mb_set_user_meta( $params );
    }
} );