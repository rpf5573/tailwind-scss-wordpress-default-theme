<?php

// 데스크톱 메뉴 출력 함수
function ex_desktop_menu_shortcode() {
    ob_start();

    wp_nav_menu(array(
      'theme_location' => 'primary',
      'container_class' => 'desktop-menu-container',
    ));

    return ob_get_clean();
}
add_shortcode( 'ex_desktop_menu', 'ex_desktop_menu_shortcode' );

// 사이트 로고
function ex_site_logo_shortcode() {
  $logo_url = EX_MB::get_site_logo_url();
  ob_start();
  ?>
  <div class="site-logo-container">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
      <img src="<?php echo $logo_url; ?>" alt="사이트 로고" class="logo-img">
    </a>
  </div>
  <?php
  return ob_get_clean();
}
add_shortcode( 'ex_site_logo', 'ex_site_logo_shortcode' );

// 모바일 사이트 로고
function ex_site_mobile_logo_shortcode() {
  $logo_url = EX_MB::get_site_mobile_logo_url();
  ob_start();
  ?>
  <div class="site-mobile-logo-container">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
      <img src="<?php echo $logo_url; ?>" alt="모바일 사이트 로고" class="logo-img">
    </a>
  </div>
  <?php
  return ob_get_clean();
}
add_shortcode( 'ex_site_mobile_logo', 'ex_site_mobile_logo_shortcode' );

// 모바일 메뉴 출력 함수
function ex_mobile_menu_shortcode() {
  ob_start();
  ?>
    <div id="mobile-menu-open-trigger" class="header-icon mobile-menu-open-trigger">
      <div class="burger-icon">
        <span class="burger-icon-top"></span>
        <span class="burger-icon-bottom"></span>
      </div>
    </div>
    <?php
  return ob_get_clean();
}
add_shortcode( 'ex_mobile_menu', 'ex_mobile_menu_shortcode' );

// 오프캔버스 내비게이션
function ex_offcanvas_navigation() {
    ob_start();
    $logo_url = EX_MB::get_site_offcanvas_logo_url();
    ?>
    <div class="offcanvas-nav-container">
      <div class="offcanvas-inner">
        <div class="offcanvas-nav-header tw-flex tw-justify-between tw-items-center tw-p-4">
          <div class="offcanvas-logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
              <img src="<?php echo $logo_url; ?>" alt="오프캔버스 로고" class="logo-img">
            </a>
          </div>
          <div id="mobile-menu-close-trigger" class="header-icon mobile-menu-close-trigger">
            <img src="<?php echo WPM_Helpers::get_icon_url('x.svg'); ?>" alt="메뉴 닫기" class="close-icon">
          </div>
        </div>
        <?php
        wp_nav_menu(array(
            'theme_location'  => 'primary',
            'container_class' => 'offcanvas-menu-container',
            'walker'          => new Offcanvas_Nav_Walker()
        )); ?>
      </div>
    </div>
    <div class="dimmed-layer"></div>
    <?php
    echo ob_get_clean();
}
add_action( 'wp_footer', 'ex_offcanvas_navigation');

// 공지사항 숏코드
function ex_notice_board_shortcode() {
  echo do_shortcode( '[kboard id=1]' );
}
add_shortcode( 'ex_notice_board', 'ex_notice_board_shortcode' );

// 행사 사진 숏코드
function ex_event_photo_shortcode() {
  echo do_shortcode( '[kboard id=2]' );
}
add_shortcode( 'ex_event_photo', 'ex_event_photo_shortcode' );

// 맛집 게시판 숏코드
function ex_restaurant_shortcode() {
  if ( isset($_GET['mod']) && ($_GET['mod'] === 'list' || $_GET['mod'] === 'document' || $_GET['mod'] === 'editor') ) {
    echo do_shortcode( '[kboard id=3]' );
    return;
  }

  $board = new KBoard(3);
  $국가_리스트 = $board->tree_category->getCategoryItemList();
  ?>

  <div class="matzip-country-list-container country-list-container">
    <ul class="matzip-country-list country-list">
      <?php foreach ($국가_리스트 as $국가) { ?>
        <li class="matzip-country-item country-item">
          <?php
          $base_url = home_url('맛집'); // '맛집' 페이지의 기본 URL 설정
          $url = $base_url . "?mod=list&pageid=1&kboard_search_option%5Btree_category_1%5D%5Bkey%5D=tree_category_1&kboard_search_option%5Btree_category_1%5D%5Bvalue%5D=" . urlencode($국가['category_name']); // 각 국가 이름을 URL 매개변수로 사용
          ?>
          <a class="matzip-country-button country-button" href="<?php echo $url; ?>" data-country="<?php echo $국가['category_name']; ?>"><?php echo $국가['category_name']; ?></a>
        </li>
      <?php } ?>
    </ul>
  </div>

  <?php
}
add_shortcode( 'ex_restaurant', 'ex_restaurant_shortcode' );

// 호텔 게시판 숏코드
function ex_hotel_shortcode() {
  if ( isset($_GET['mod']) && ($_GET['mod'] === 'list' || $_GET['mod'] === 'document' || $_GET['mod'] === 'editor') ) {
    echo do_shortcode( '[kboard id=4]' );
    return;
  }

  $board = new KBoard(4);
  $국가_리스트 = $board->tree_category->getCategoryItemList();
  ?>

  <div class="hotel-country-list-container country-list-container">
    <ul class="hotel-country-list country-list">
      <?php foreach ($국가_리스트 as $국가) { ?>
        <li class="hotel-country-item country-item">
          <?php
          $base_url = home_url('호텔'); // '맛집' 페이지의 기본 URL 설정
          $url = $base_url . "?mod=list&pageid=1&kboard_search_option%5Btree_category_1%5D%5Bkey%5D=tree_category_1&kboard_search_option%5Btree_category_1%5D%5Bvalue%5D=" . urlencode($국가['category_name']); // 각 국가 이름을 URL 매개변수로 사용
          ?>
          <a class="hotel-country-button country-button" href="<?php echo $url; ?>" data-country="<?php echo $국가['category_name']; ?>"><?php echo $국가['category_name']; ?></a>
        </li>
      <?php } ?>
    </ul>
  </div>

  <?php
}
add_shortcode( 'ex_hotel', 'ex_hotel_shortcode' );

// 쇼핑 게시판 숏코드
function ex_shopping_shortcode() {
  if ( isset($_GET['mod']) && ($_GET['mod'] === 'list' || $_GET['mod'] === 'document' || $_GET['mod'] === 'editor') ) {
    echo do_shortcode( '[kboard id=5]' );
    return;
  }

  $board = new KBoard(5);
  $국가_리스트 = $board->tree_category->getCategoryItemList();
  ?>

  <div class="shopping-country-list-container country-list-container">
    <ul class="shopping-country-list country-list">
      <?php foreach ($국가_리스트 as $국가) { ?>
        <li class="shopping-country-item country-item">
          <?php
          $base_url = home_url('쇼핑'); // '맛집' 페이지의 기본 URL 설정
          $url = $base_url . "?mod=list&pageid=1&kboard_search_option%5Btree_category_1%5D%5Bkey%5D=tree_category_1&kboard_search_option%5Btree_category_1%5D%5Bvalue%5D=" . urlencode($국가['category_name']); // 각 국가 이름을 URL 매개변수로 사용
          ?>
          <a class="shopping-country-button country-button" href="<?php echo $url; ?>" data-country="<?php echo $국가['category_name']; ?>"><?php echo $국가['category_name']; ?></a>
        </li>
      <?php } ?>
    </ul>
  </div>

  <?php
}
add_shortcode( 'ex_shopping', 'ex_shopping_shortcode' );

add_action( 'wp_footer', function() { ?>
  <div class="login-modal">
    <?php echo do_shortcode( '[mb_user_profile_login]' ); ?>
  </div>

  <!-- Link to open the modal -->
  <?php
} );

// 회원 전용 페이지 안내문구
function ex_member_only_notice() { ?>
  <div class="tw-flex tw-justify-center">
    <div class="tw-text-center tw-text-3xl tw-font-bold tw-p-4 tw-text-purple-900">회원 전용 서비스이므로 비회원에게는 공개되지 않습니다.</div>
  </div> <?php
}
add_shortcode( 'ex_member_only_notice', 'ex_member_only_notice' );